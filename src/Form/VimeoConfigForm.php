<?php

/**
 * @file
 * Contains \Drupal\vimeo_api\Form\VimeoConfigForm.
 */

namespace Drupal\vimeo_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Vimeo\Vimeo;

/**
 * Configure the Vimeo API for this site.
 */
class VimeoConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vimeo_api.api'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    global $base_url;
    $redirect_uri = $base_url . '/admin/config/services/vimeo-api';

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#default_value' => (!empty($config->get('client_id'))) ? $config->get('client_id') : '',
      '#description' => t('The Vimeo Client ID. Can be obtained from <a href="https://developer.vimeo.com/apps" target="_blank">here</a>, clicking on the desired app and go to the "Authentication" tab.'),
      '#required' => TRUE,
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#default_value' => (!empty($config->get('client_secret'))) ? $config->get('client_secret') : '',
      '#description' => t('The Vimeo Client Secrets. Similarly as above, can be obtained from <a href="https://developer.vimeo.com/apps" target="_blank">here</a>, clicking on the desired app and go to the "Authentication" tab.'),
      '#required' => TRUE,
    );

    $vimeo_lib = NULL;
    if (!empty($config->get('client_id')) && !empty($config->get('client_secret'))) {
      $vimeo_lib = new Vimeo($config->get('client_id'), $config->get('client_secret'));
    }

    if ($vimeo_lib) {
      // Check if we are in a callback.
      $request = \Drupal::request();
      $query_params = $request->query;
      // If we have the same 'state' from the callback
      // as well 'code' in the query params, Vimeo
      // replied positively to the auth URL call.
      $valid_state = ($query_params->get('state') && $query_params->get('state') == $config->get('auth_state'));
      if ($valid_state && $query_params->get('code')) {
        // Proceed to retrieving the access token
        $token = $vimeo_lib->accessToken($query_params->get('code'), $redirect_uri);
        if (!empty($token['body']['access_token'])) {
          $config->set('access_token', $token['body']['access_token']);
          $config->clear('auth_state');
          $config->save();
        }
      }
      // Otherwise generate the AUTH url.
      elseif ($vimeo_lib && empty($config->get('access_token'))) {
        // Generate API params.
        $scopes = array('private', 'upload');
        $uuid_service = \Drupal::service('uuid');
        $state = $uuid_service->generate();
        $config->set('auth_state', $state);
        $config->save();

        // Generate the Authorize URL.
        $authorize_url = $vimeo_lib->buildAuthorizationEndpoint($redirect_uri, $scopes, $state);

        $text = 'Authorisation URL';
        $link = Link::fromTextAndUrl($text, Url::fromUri($authorize_url));
        $form['auth_url'] = array(
          '#type' => 'markup',
          '#markup' => $link->toString(),
        );
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    $config->set('client_id', $form_state->getValue('client_id'));
    $config->set('client_secret', $form_state->getValue('client_secret'));
    $config->save();
    parent::submitForm($form, $form_state);
  }
}