<?php
/**
 * @file
 * Contains \Drupal\vimeo_api\Form\VimeoUploadForm.
 */

namespace Drupal\vimeo_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Vimeo\Vimeo;

class VimeoUploadForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_api_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    if (vimeo_api_check_api_config($config)) {
      $vimeo_lib = new Vimeo($config->get('client_id'), $config->get('client_secret'));
      $vimeo_lib->setToken($config->get('access_token'));
      $response = $vimeo_lib->request('/me', array(), 'GET');

      $text = $response['body']['name'];
      $link = Link::fromTextAndUrl($text, Url::fromUri($response['body']['link'], array('attributes' => array('target' => '_blank'))));
      $form['account_url'] = array(
        '#type' => 'markup',
        '#markup' => $link->toString(),
        '#prefix' => '<div>Vimeo Account: ',
        '#suffix' => '</div>'
      );

      $form['url'] = array(
        '#type' => 'url',
        '#title' => t('Upload URL'),
        '#description' => t('Enter a URL to a video that you want to upload to Vimeo. Vimeo will automatically upload the video from that URL.'),
        '#required' => TRUE,
      );

      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Upload'),
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    if (vimeo_api_check_api_config($config)) {
      $vimeo_lib = new Vimeo($config->get('client_id'), $config->get('client_secret'));
      $vimeo_lib->setToken($config->get('access_token'));

      $url = $form_state->getValue('url');
      $response = $vimeo_lib->request('/me/videos', array('type' => 'pull', 'link' => $url), 'POST');
      if ($response['status'] == 200) {
        $text = $response['body']['name'];
        $link = Link::fromTextAndUrl($text, Url::fromUri($response['body']['link'], array('attributes' => array('target' => '_blank'))));
        drupal_set_message('Video created: ' . $link->toString());
      }
    }
  }
}